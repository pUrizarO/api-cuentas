@validar-cuentas
Feature: Validar cuentas del cliente

	@con-cuentas
  Scenario Outline: Cliente con cuentas
    Given hago un get a "/api/clientes/" con parametro id de valor <id>
    Then compruebo que la propiedad "id" no esta vacia ni nulo
    And compruebo que la propiedad "apellidos" tiene de valor "<apellidos>"
    And compruebo que la propiedad "nombres" no esta vacia ni nulo
    And compruebo que la propiedad "nombres" tiene de valor "<nombres>"
    And compruebo que la propiedad "email" tiene de valor "<email>"
    And compruebo que el array "cuentas" es mayor que 0

    Examples: 
      | id | apellidos | nombres | email          |
      |  1 | Cenas     | Dany    | dany@mail.com  |
      |  3 | Medina    | Jaime   | jaime@mail.com |
