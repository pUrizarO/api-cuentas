package com.mitocode.rest.controller;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.UUID;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mitocode.model.Cliente;
import com.mitocode.model.Cuenta;
import com.mitocode.model.TipoCuenta;
import com.mitocode.model.Transaccion;

@RestController
@RequestMapping("/api/clientes")
public class ClienteRest {

	private static ArrayList<Cliente> clientes = new ArrayList<>();
	private static ArrayList<Cuenta> cuentas = new ArrayList<>();
	private static ArrayList<Transaccion> transacciones = new ArrayList<>();

	static {

		Transaccion t1 = new Transaccion(UUID.randomUUID(), "Uber", LocalDate.now(), new BigDecimal(20.00).setScale(2));
		Transaccion t2 = new Transaccion(UUID.randomUUID(), "Linio", LocalDate.now(), new BigDecimal(50.00).setScale(2));
		transacciones.add(t1);
		transacciones.add(t2);

		Cuenta c1 = new Cuenta(UUID.randomUUID(), "570-000000-0-10", new BigDecimal(1000.00).setScale(2), TipoCuenta.SOLES,
				new ArrayList<>());
		Cuenta c2 = new Cuenta(UUID.randomUUID(), "570-000000-0-11", new BigDecimal(3000.00).setScale(2), TipoCuenta.DOLARES,
				new ArrayList<>());
		Cuenta c3 = new Cuenta(UUID.randomUUID(), "570-000000-0-12", new BigDecimal(2000.00).setScale(2), TipoCuenta.EUROS,
				new ArrayList<>());
		cuentas.add(c1);
		cuentas.add(c2);
		cuentas.add(c3);

		Cliente u1 = new Cliente(1L, "Cenas", "Dany", "dany@mail.com", cuentas);
		Cliente u2 = new Cliente(2L, "Perez", "Juan", "juan@mail.com", new ArrayList<>());

		cuentas = new ArrayList<>();
		Cuenta cx = new Cuenta(UUID.randomUUID(), "570-000000-0-14", new BigDecimal(1500.00).setScale(2), TipoCuenta.SOLES,
				transacciones);
		cuentas.add(cx);

		Cliente u3 = new Cliente(3L, "Medina", "Jaime", "jaime@mail.com", cuentas);
		Cliente u4 = new Cliente(4L, "Mundo", "Hola", "hola@mail.com", new ArrayList<>());
		clientes.add(u1);
		clientes.add(u2);
		clientes.add(u3);
		clientes.add(u4);
	}

	@GetMapping
	public ArrayList<Cliente> listarTodos() {
		return clientes;
	}

	@GetMapping("/{id}")
	public Cliente obtenerPorId(@PathVariable Long id) {
		return clientes.stream().filter(e -> e.getId() == id).findFirst().orElse(null);
	}

}
